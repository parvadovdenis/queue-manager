package com.parvadov.denys.queuemanager.engines;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Contacts;
import android.util.Log;

import com.parvadov.denys.queuemanager.activities.MainActivity;
import com.parvadov.denys.queuemanager.activities.SettingsActivity;
import com.parvadov.denys.queuemanager.receivers.CallReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.Date;

/**
 * Created by denys on 09.04.17.
 */

public class CallServiceEngine {
    private static CallServiceEngine callServiceEngine;
    private Context context;
    private SharedPreferences sharedPref;

    private CallServiceEngine(Context context, SharedPreferences sharedPref) {
        this.context = context;
        this.sharedPref = sharedPref;
    }

    public static CallServiceEngine getInstance(Context context, SharedPreferences sharedPref) {
        if (callServiceEngine == null) {
            callServiceEngine = new CallServiceEngine(context, sharedPref);
            return callServiceEngine;
        } else {
            return callServiceEngine;
        }
    }

    public String getContactName(final String phoneNumber) {
        Uri uri;
        String[] projection;
        Uri mBaseUri = Contacts.Phones.CONTENT_FILTER_URL;
        projection = new String[]{android.provider.Contacts.People.NAME};
        try {
            Class<?> c = Class.forName("android.provider.ContactsContract$PhoneLookup");
            mBaseUri = (Uri) c.getField("CONTENT_FILTER_URI").get(mBaseUri);
            projection = new String[]{"display_name"};
        } catch (Exception e) {
        }

        uri = Uri.withAppendedPath(mBaseUri, Uri.encode(phoneNumber));
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);

        String contactName = "";

        if (cursor.moveToFirst()) {
            contactName = cursor.getString(0);
        }

        cursor.close();
        cursor = null;

        return contactName;
    }

    public boolean isUserAlreadyInQueue(String phone) {
        JSONArray array = null;
        String json = sharedPref.getString(MainActivity.CUSTOMER_LIST, "null");
        JSONObject result = null;
        Date date = new Date();

        try {
            if (!json.equals("null")) {
                array = new JSONArray(json);
            } else {
                array = new JSONArray();
            }
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = null;
                Date jsonDate = null;

                object = array.getJSONObject(i);
                jsonDate = new Date(object.getLong("date") * 1000);
                result = null;
                if (date.getYear() == jsonDate.getYear()
                        && date.getMonth() == jsonDate.getMonth()
                        && date.getDay() == jsonDate.getDay()) {
                    result = object;
                }
            }

            if (result != null) {
                JSONArray customersList = result.getJSONArray("list");
                for (int i = 0; i < customersList.length(); i++) {
                    if (phone.equals(customersList.getJSONObject(i).getString("phone"))) {
                        return true;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

        return false;
    }

    public void disconnectCall() {
        try {
            String serviceManagerName = "android.os.ServiceManager";
            String serviceManagerNativeName = "android.os.ServiceManagerNative";
            String telephonyName = "com.android.internal.telephony.ITelephony";
            Class<?> telephonyClass;
            Class<?> telephonyStubClass;
            Class<?> serviceManagerClass;
            Class<?> serviceManagerNativeClass;
            Method telephonyEndCall;
            Object telephonyObject;
            Object serviceManagerObject;
            telephonyClass = Class.forName(telephonyName);
            telephonyStubClass = telephonyClass.getClasses()[0];
            serviceManagerClass = Class.forName(serviceManagerName);
            serviceManagerNativeClass = Class.forName(serviceManagerNativeName);
            Method getService = // getDefaults[29];
                    serviceManagerClass.getMethod("getService", String.class);
            Method tempInterfaceMethod = serviceManagerNativeClass.getMethod("asInterface", IBinder.class);
            Binder tmpBinder = new Binder();
            tmpBinder.attachInterface(null, "fake");
            serviceManagerObject = tempInterfaceMethod.invoke(null, tmpBinder);
            IBinder retbinder = (IBinder) getService.invoke(serviceManagerObject, "phone");
            Method serviceMethod = telephonyStubClass.getMethod("asInterface", IBinder.class);
            telephonyObject = serviceMethod.invoke(null, retbinder);
            telephonyEndCall = telephonyClass.getMethod("endCall");
            telephonyEndCall.invoke(telephonyObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isTargetSim(Intent intent, SharedPreferences sharedPref) {
        boolean isTargetSim = true;
        try {
            if (sharedPref.getBoolean(SettingsActivity.IS_DUAL_SIM, false)) {
                int chosenSimId = sharedPref.getInt(SettingsActivity.SIM_ID, -1);
                if (chosenSimId != -1) {
                    Bundle bundle = intent.getExtras();
                    int phoneId = bundle.getInt(CallReceiver.SIM_ID, -1);
                    Log.i("denys", "target sim is " + phoneId);
                    if (chosenSimId != phoneId) {
                        isTargetSim = false;
                    }
                }
            }
        } catch (Exception e) {
            isTargetSim = true;
        } finally {
            return isTargetSim;
        }
    }
}
