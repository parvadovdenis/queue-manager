package com.parvadov.denys.queuemanager.engines;

import android.content.SharedPreferences;

/**
 * Created by denys on 09.04.17.
 */

public abstract class BaseEngine {
    protected SharedPreferences sharedPref;
    protected SharedPreferences.Editor editor;

    protected BaseEngine(SharedPreferences sharedPref, SharedPreferences.Editor editor) {
        this.sharedPref = sharedPref;
        this.editor = editor;
    }

    protected BaseEngine(SharedPreferences sharedPreferences) {
        this.sharedPref = sharedPreferences;

    }

}
