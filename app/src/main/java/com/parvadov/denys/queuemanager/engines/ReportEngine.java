package com.parvadov.denys.queuemanager.engines;

import android.content.SharedPreferences;
import android.util.Log;

import com.parvadov.denys.queuemanager.activities.MainActivity;
import com.parvadov.denys.queuemanager.models.ReportDaily;
import com.parvadov.denys.queuemanager.models.ReportWeekly;
import com.parvadov.denys.queuemanager.models.ReportYearly;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by denys on 09.04.17.
 */

public class ReportEngine extends BaseEngine {
    private static ReportEngine reportEngine;

    public static ReportEngine getInstance(SharedPreferences sharedPreferences) {
        if (reportEngine == null) {
            reportEngine = new ReportEngine(sharedPreferences);
            return reportEngine;
        } else {
            return reportEngine;
        }
    }


    private ReportEngine(SharedPreferences sharedPreferences) {
        super(sharedPreferences);
    }

    public ArrayList<ReportYearly> getYearlyReportList(int year) {
        ArrayList<ReportYearly> result = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            result.add(new ReportYearly(0, i, year));
        }
        JSONArray array = null;
        String json = sharedPref.getString(MainActivity.CUSTOMER_LIST, "null");
        Log.i("denys", json);
        try {
            if (!json.equals("null")) {
                array = new JSONArray(json);
            }
            for (ReportYearly reportYearly : result) {
                int count = 0;
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    Date date = new Date(object.getLong(MainActivity.DATE) * 1000);
                    if (reportYearly.getIntYear() == date.getYear() && reportYearly.getIntMonth() == date.getMonth()) {
                        count += object.getJSONArray(MainActivity.LIST).length();
                    }
                }
                reportYearly.setCustomersCount(count);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        }
        return result;
    }

    public ArrayList<ReportWeekly> getWeeklyReportList(Date date) {
        Calendar calendar = Calendar.getInstance();
        Calendar calendarReportWeekly = Calendar.getInstance();
        calendarReportWeekly.setTime(date);
        ArrayList<ReportWeekly> result = new ArrayList<>();
        for (int i = 1; i <= calendarReportWeekly.getActualMaximum(Calendar.WEEK_OF_MONTH); i++) {
            result.add(new ReportWeekly(i, 0));
        }
        JSONArray array = null;
        String json = sharedPref.getString(MainActivity.CUSTOMER_LIST, "null");
        try {
            if (!json.equals("null")) {
                array = new JSONArray(json);
            }
            for (ReportWeekly reportWeekly : result) {
                int count = 0;
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    Date jsonObjectDate = new Date(object.getLong(MainActivity.DATE) * 1000L);
                    calendar.setTime(jsonObjectDate);
                    if (calendarReportWeekly.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                            calendarReportWeekly.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                            reportWeekly.getWeekNumber() == calendar.get(Calendar.WEEK_OF_MONTH)) {
                        count += object.getJSONArray(MainActivity.LIST).length();
                    }
                }
                reportWeekly.setCustomersCount(count);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        }
        return result;
    }

    public ArrayList<ReportDaily> getDailyReportList(Date date) {
        Calendar calendar = Calendar.getInstance();
        Calendar calendarReportDaily = Calendar.getInstance();
        calendarReportDaily.setTime(date);
        ArrayList<ReportDaily> result = new ArrayList<>();
        for (int i = 1; i <= 7; i++) {
            Calendar c = Calendar.getInstance();
            c = calendarReportDaily;
            c.set(Calendar.DAY_OF_WEEK, i);
            Date reportDate = c.getTime();
            if (date.getMonth() == calendarReportDaily.get((Calendar.MONTH)))
                result.add(new ReportDaily(calendarReportDaily.get(Calendar.YEAR), calendarReportDaily.get(Calendar.MONTH),
                        i, 0, calendar.get(Calendar.WEEK_OF_MONTH), reportDate));
        }
        JSONArray array = null;
        String json = sharedPref.getString(MainActivity.CUSTOMER_LIST, "null");
        Log.i("denys", json);
        try {
            if (!json.equals("null")) {
                array = new JSONArray(json);
            }
            for (ReportDaily reportDaily : result) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    Date jsonObjectDate = new Date(object.getLong(MainActivity.DATE) * 1000L);
                    calendar.setTime(jsonObjectDate);
                    if (calendarReportDaily.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                            calendarReportDaily.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                            calendarReportDaily.get(Calendar.WEEK_OF_MONTH) == calendar.get(Calendar.WEEK_OF_MONTH) &&
                            reportDaily.getDay() == calendar.get(Calendar.DAY_OF_WEEK)) {
                        reportDaily.setCustomersCount(object.getJSONArray(MainActivity.LIST).length());
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        }
        return result;
    }

    public int createYearlyReport() {
        int result = 0;
        JSONArray array = null;
        String json = sharedPref.getString(MainActivity.CUSTOMER_LIST, "null");
        Calendar calendar = Calendar.getInstance();
        Log.i("denys", json);
        try {
            if (!json.equals("null")) {
                array = new JSONArray(json);
            }
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = null;
                Calendar jsonDate = null;
                object = array.getJSONObject(i);
                jsonDate = Calendar.getInstance();
                jsonDate.setTime(new Date(object.getLong("date") * 1000));
                Log.i("denys", "week of item is " + jsonDate.YEAR);
                if (calendar.YEAR == jsonDate.YEAR) {
                    result += object.getJSONArray(MainActivity.LIST).length();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result = 0;
        } catch (NullPointerException e) {
            result = 0;
        }
        return result;

    }

    public int createDailyReport() {
        int result = 0;
        JSONArray array = null;
        String json = sharedPref.getString(MainActivity.CUSTOMER_LIST, "null");
        Date date = new Date();
        try {
            if (!json.equals("null")) {
                array = new JSONArray(json);
            }
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = null;
                Date jsonDate = null;
                object = array.getJSONObject(i);
                jsonDate = new Date(object.getLong(MainActivity.DATE) * 1000);
                if (date.getYear() == jsonDate.getYear()
                        && date.getMonth() == jsonDate.getMonth()
                        && date.getDay() == jsonDate.getDay()) {
                    result = object.getJSONArray(MainActivity.LIST).length();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result = 0;
        } catch (NullPointerException e) {
            result = 0;
        }
        return result;
    }

    public int createWeeklyReport() {
        int result = 0;
        JSONArray array = null;
        String json = sharedPref.getString(MainActivity.CUSTOMER_LIST, "null");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        Log.i("denys", "calendar this week is " + String.valueOf(calendar.WEEK_OF_YEAR));
        try {
            if (!json.equals("null")) {
                array = new JSONArray(json);
            }
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = null;
                Calendar jsonDate = null;
                object = array.getJSONObject(i);
                jsonDate = Calendar.getInstance();
                jsonDate.setTime(new Date(object.getLong("date") * 1000));
                Log.i("denys", "week of item is " + jsonDate.WEEK_OF_YEAR);
                if (calendar.get(Calendar.WEEK_OF_YEAR) == jsonDate.get(Calendar.WEEK_OF_YEAR)
                        && calendar.get(Calendar.YEAR) == jsonDate.get((Calendar.YEAR))) {
                    result += object.getJSONArray(MainActivity.LIST).length();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result = 0;
        } catch (NullPointerException e) {
            result = 0;
        }
        return result;
    }
}
