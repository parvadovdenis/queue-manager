package com.parvadov.denys.queuemanager.models;

import java.util.Date;

/**
 * Created by Denys Parvadov on 05.03.17.
 */

public class Customer {
    private String phone;
    private String name;
    private int number;
    private Date dateCreate;

    public Customer(String phone, String name, int number) {
        this.phone = phone;
        this.name = name;
        this.number = number;
        this.dateCreate = new Date();
    }

    public Customer() {

    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public void setDateCreate(Long dateCreate) {
        this.dateCreate = new Date(dateCreate * 1000);
    }
}
