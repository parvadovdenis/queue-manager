package com.parvadov.denys.queuemanager.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;

import com.parvadov.denys.queuemanager.activities.MainActivity;
import com.parvadov.denys.queuemanager.activities.SettingsActivity;
import com.parvadov.denys.queuemanager.engines.CallServiceEngine;
import com.parvadov.denys.queuemanager.engines.UserEngine;
import com.parvadov.denys.queuemanager.receivers.CallReceiver;

import java.sql.Time;
import java.util.Date;

/**
 * Created by denys on 12.03.17.
 */

public class CallService extends Service {
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;
    private Intent mEmptyIntent;
    private static SharedPreferences sharedPref;
    private static SharedPreferences.Editor editor;
    private static final String SHARED = "QueueManager_Preferences";
    private UserEngine userEngine;
    private CallServiceEngine callServiceEngine;

    private boolean isInTimeSchedule(SharedPreferences sharedPref) {
        boolean result;
        String autoOn = sharedPref.getString(SettingsActivity.AUTO_TURN_ON, "");
        String autoOff = sharedPref.getString(SettingsActivity.AUTO_TURN_OFF, "");
        if ("".equals(autoOn) || "".equals(autoOff)) result = true;
        else {
            String[] arrAutoOn = autoOn.split(" : ");
            String[] arrAutoOff = autoOff.split(" : ");
            Time autoOnTime = new Time(Integer.parseInt(arrAutoOn[0]), Integer.parseInt(arrAutoOn[1]), 0);
            Time autoOffTime = new Time(Integer.parseInt(arrAutoOff[0]), Integer.parseInt(arrAutoOff[1]), 0);
            Date currentDate = new Date();
            Time currentTime = new Time(currentDate.getHours(), currentDate.getMinutes(), 0);
            if (autoOnTime.getTime() < currentTime.getTime() && currentTime.getTime() < autoOffTime.getTime())
                result = true;
            else result = false;
        }
        return result;
    }

    private void startWork(Intent intent) {
        Log.i("denys", "start service");
        callServiceEngine.disconnectCall();
        String phone = intent.getStringExtra(CallReceiver.PHONE);
        String number = userEngine.getPreviousCustomerNumber(sharedPref);
        new ServiceTask().execute(phone, number);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPref = getBaseContext().getSharedPreferences(SHARED, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        userEngine = UserEngine.getInstance(sharedPref, editor);
        callServiceEngine = CallServiceEngine.getInstance(getBaseContext(), sharedPref);
        if (callServiceEngine.isTargetSim(intent, sharedPref))
            if (sharedPref.getBoolean(SettingsActivity.POWER, false)) {
                if (sharedPref.getBoolean(SettingsActivity.AUTO_SCHEDULE, false)) {
                    if (isInTimeSchedule(sharedPref)) {
                        startWork(intent);
                    }
                } else {
                    startWork(intent);
                }
            }
        return START_NOT_STICKY;
    }

    private class ServiceTask extends AsyncTask<String, Object, Boolean> {
        String number;
        String phone;

        @Override
        protected Boolean doInBackground(String... params) {
            phone = params[0];
            if (callServiceEngine.isUserAlreadyInQueue(phone)) {
                return false;
            } else {
                number = params[1];
                String name = callServiceEngine.getContactName(phone);
                userEngine.saveCustomer(phone, name, number);
                return true;
            }
        }

        @Override
        protected void onPostExecute(final Boolean result) {
            String interval = sharedPref.getString(SettingsActivity.INTERVAL, null);
            String message = "";
            if (result) {
                message = "Your number is " + number;
                if (interval != null)
                    message += ", expected time is " + Integer.parseInt(number) * Integer.parseInt(interval) + " min.";
                sendNotification(phone);
            } else {
                message = "Your number is already in list";
            }
            if (sharedPref.getBoolean(SettingsActivity.SEND_SMS, false)) {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phone, null, message, null, null); //TODO set expected time bu interval
            }
            MainActivity.updateList();
            onDestroy();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void sendNotification(final String phone) {
        new AsyncTask<Object, Object, Object>() {
            @Override
            protected void onPreExecute() {
                mEmptyIntent = new Intent(getBaseContext(), MainActivity.class);
                mEmptyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                mNotifyManager =
                        (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);
                mBuilder = new NotificationCompat.Builder(getBaseContext());
                PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, mEmptyIntent, PendingIntent.FLAG_UPDATE_CURRENT
                );
                mBuilder.setContentTitle("Queue Manager")
                        .setDefaults(Notification.DEFAULT_SOUND
                                | Notification.DEFAULT_VIBRATE
                                | Notification.DEFAULT_LIGHTS)
                        .setContentText("Customer " + phone + " was added!")
                        .setSmallIcon(android.R.drawable.ic_dialog_info)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), android.R.drawable.ic_dialog_info))
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);
                super.onPreExecute();
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                int id = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
                mNotifyManager.notify(id, mBuilder.build());
                super.onPostExecute(o);
            }
        }.execute();

    }

}
