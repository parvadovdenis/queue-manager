package com.parvadov.denys.queuemanager.receivers;

import android.content.Context;
import android.content.Intent;

import com.parvadov.denys.queuemanager.services.CallService;

import java.util.Date;

/**
 * Created by denys on 12.03.17.
 */

public class CallReceiver extends PhoneCallReceiver {
//    private static Context context;
    public static final String PHONE = "INCOMING_NUMBER";
    public static final String SIM_ID = "SIM_ID";

    public static CallReceiver newInstance(Context context) {
//        CallReceiver.context = context;
        return new CallReceiver();
    }

    public CallReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }

    @Override
    protected void onIncomingCallStarted(Context ctx, String phone, Date start, int simId) {
        Intent serviceIntent = new Intent(ctx, CallService.class);
        serviceIntent.putExtra(PHONE, phone);
        serviceIntent.putExtra(SIM_ID, simId);
        ctx.startService(serviceIntent);
    }

}
