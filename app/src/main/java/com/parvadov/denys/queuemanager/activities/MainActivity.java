package com.parvadov.denys.queuemanager.activities;

import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parvadov.denys.queuemanager.R;
import com.parvadov.denys.queuemanager.adapters.ListAdapter;
import com.parvadov.denys.queuemanager.engines.UserEngine;
import com.parvadov.denys.queuemanager.horizontalCalendar.HorizontalCalendar;
import com.parvadov.denys.queuemanager.horizontalCalendar.HorizontalCalendarListener;
import com.parvadov.denys.queuemanager.models.Customer;
import com.parvadov.denys.queuemanager.receivers.CallReceiver;
import com.parvadov.denys.queuemanager.views.myTextView.MyTextView;
import com.testfairy.TestFairy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends BaseActivity {

    public static ListAdapter adapter;
    public static Context context;
    public static ArrayList<Customer> customersList = new ArrayList<>();
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private static SharedPreferences sharedPref;
    private static SharedPreferences.Editor editor;
    public static final String CUSTOMER_LIST = "customer_list";
    private final String BROADCAST = "com.parvadov.denys.queuemanager.android.action.broadcast";
    private static final String NAME = "name";
    public static final String PHONE = "phone";
    private static final String NUMBER = "number";
    private static final String TIME = "time";
    public static final String DATE = "date";
    public static final String LIST = "list";
    private CallReceiver callReceiver;
    private static Date currentDate;
    private HorizontalCalendar horizontalCalendar;

    private static UserEngine userEngine;

    private String name;
    private String number;
    private String phone;

    private FloatingActionButton fab;
    private static boolean forceShowCustomers = false;

    @Override
    public int getContentLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void initViews() {
        sharedPref = BaseActivity.sharedPref;
        editor = BaseActivity.editor;
        userEngine = UserEngine.getInstance(sharedPref, editor);
        TestFairy.begin(this, "907cde26075076385fe80724340ccac1f0f90d72");
        if (currentDate == null) currentDate = new Date();
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(setupAddCustomerButton());
        setupHorizontalCalendar();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("test")) {
            forceShowCustomers = true;
            Date date = new Date(bundle.getLong("test") * 1000L);
            horizontalCalendar.selectDate(date, true);
            currentDate = date;
            hideFab(date);
        } else {
            horizontalCalendar.selectDate(new Date(), true);
            hideFab(new Date());
        }

        setCurrentMonthAndYear();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        updateList();
        adapter = new ListAdapter(this, customersList);
        recyclerView.setAdapter(adapter);
        callReceiver = CallReceiver.newInstance(this);
        getApplicationContext().registerReceiver(callReceiver, new IntentFilter(BROADCAST));
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = itemTouchHelper();

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private View.OnClickListener setupAddCustomerButton() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                LayoutInflater factory = getLayoutInflater();
                View dialog_add_customer = factory.inflate(R.layout.dialog_add_customer, null);
                alert.setView(dialog_add_customer);
                final AlertDialog dialog = alert.create();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                final EditText mEditPhone, mEditName, mEditNumber;
                mEditPhone = (EditText) dialog_add_customer.findViewById(R.id.editPhone);
                mEditName = (EditText) dialog_add_customer.findViewById(R.id.editName);
                mEditNumber = (EditText) dialog_add_customer.findViewById(R.id.editNumber);

                Button bAdd = (Button) dialog_add_customer.findViewById(R.id.buttonAdd);
                bAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        name = String.valueOf(mEditName.getText());
                        phone = String.valueOf(mEditPhone.getText());
                        number = mEditNumber.getText().toString();
                        if (number.isEmpty()) {
                            number = "0";
                        }
                        userEngine.saveCustomer(phone, name, number);
                        horizontalCalendar.selectDate(new Date(), false);
                        updateList();
                        dialog.dismiss();
                    }
                });

                MyTextView cancelIcon = (MyTextView) dialog_add_customer.findViewById(R.id.cancelIcon);
                cancelIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        };
    }

    private ItemTouchHelper.SimpleCallback itemTouchHelper() {
        return new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                Toast.makeText(MainActivity.this, "Removed", Toast.LENGTH_SHORT).show();
                Customer customerForRemove = customersList.get(position);
                UserEngine.currentDate = customersList.get(position).getDateCreate();
                customersList.remove(position);
                userEngine.removeCustomer(customerForRemove);
                adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                //Remove swiped item from list and notify the RecyclerView
            }
        };
    }

    private void setupHorizontalCalendar() {
        /** end after 1 month from now */
        Calendar endDate = Calendar.getInstance();
//        endDate.add(Calendar.MONTH, 1);

        /** start before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -100);

        horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .centerToday(false)
                .build();
        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {
                forceShowCustomers = false;
                hideFab(date);
                currentDate = date;
                updateList();
            }
        });
    }

    private void hideFab(Date date) {
        Calendar selectedDate = Calendar.getInstance();
        Calendar currDate = Calendar.getInstance();
        selectedDate.setTime(date);
        currDate.setTime(new Date());

        if (currDate.get(Calendar.YEAR) != selectedDate.get(Calendar.YEAR) ||
                currDate.get(Calendar.MONTH) != selectedDate.get(Calendar.MONTH) ||
                currDate.get(Calendar.DAY_OF_MONTH) != selectedDate.get(Calendar.DAY_OF_MONTH)) {
            fab.setVisibility(View.GONE);
        } else {
            fab.setVisibility(View.VISIBLE);
        }
    }

    private void setCurrentMonthAndYear() {
        SimpleDateFormat mDateFormat = new SimpleDateFormat("MMMM, y");
        MyTextView mCurrentMonthAndYear = (MyTextView) findViewById(R.id.currentMonthAndYear);
        mCurrentMonthAndYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horizontalCalendar.goToday(false);
                updateList();
            }
        });
        Date mCurrentDate = new Date();
        String mStringCurrentMonthAndYear = mDateFormat.format(mCurrentDate);
        mCurrentMonthAndYear.setText("Today - " + mStringCurrentMonthAndYear);
    }

    public static void updateList() {
        if (sharedPref == null) return;
        JSONArray array = null;
        customersList.clear();
        String json = sharedPref.getString(CUSTOMER_LIST, "");
        try {
            if (!json.isEmpty()) {
                array = new JSONArray(json);
                JSONObject currentDateJsonObject = userEngine.getDateJsonObjectFromArray(array, currentDate);
                if (currentDateJsonObject == null) {
                    if (adapter != null)
                        adapter.notifyDataSetChanged();
                    return;
                }
                JSONArray listArray = currentDateJsonObject.getJSONArray(LIST);
                for (int i = 0; i < listArray.length(); i++) {
                    JSONObject customer = listArray.getJSONObject(i);
                    if (customer.has(UserEngine.NOT_SHOW_IN_LIST)) {
                        if (!customer.getBoolean(UserEngine.NOT_SHOW_IN_LIST)) {
                            String phone, name;
                            int number;
                            long time;
                            phone = customer.getString(PHONE);
                            name = customer.getString(NAME);
                            number = Integer.parseInt(customer.getString(NUMBER));
                            time = Long.parseLong(customer.getString(TIME));
                            Customer newCustomer = new Customer(phone, name, number);
                            newCustomer.setDateCreate(time);
                            customersList.add(newCustomer);
                        } else if(forceShowCustomers){
                            String phone, name;
                            int number;
                            long time;
                            phone = customer.getString(PHONE);
                            name = customer.getString(NAME);
                            number = Integer.parseInt(customer.getString(NUMBER));
                            time = Long.parseLong(customer.getString(TIME));
                            Customer newCustomer = new Customer(phone, name, number);
                            newCustomer.setDateCreate(time);
                            customersList.add(newCustomer);
                        }
                    } else {
                        String phone, name;
                        int number;
                        long time;
                        phone = customer.getString(PHONE);
                        name = customer.getString(NAME);
                        number = Integer.parseInt(customer.getString(NUMBER));
                        time = Long.parseLong(customer.getString(TIME));
                        Customer newCustomer = new Customer(phone, name, number);
                        newCustomer.setDateCreate(time);
                        customersList.add(newCustomer);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

}
