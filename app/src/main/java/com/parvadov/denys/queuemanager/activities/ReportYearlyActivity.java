package com.parvadov.denys.queuemanager.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.parvadov.denys.queuemanager.R;
import com.parvadov.denys.queuemanager.adapters.ReportYearlyAdapter;
import com.parvadov.denys.queuemanager.engines.ReportEngine;
import com.parvadov.denys.queuemanager.models.ReportYearly;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by denys on 11.04.17.
 */

public class ReportYearlyActivity extends BaseActivity implements ReportYearlyAdapter.OnClickListener {
    private final int layout = R.layout.activity_report_yearly;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<ReportYearly> reportYearlyList;
    private SharedPreferences sharedPreferences;
    int year;

    @Override
    public int getContentLayout() {
        return layout;
    }

    @Override
    public void initViews() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("year")) {
            year = bundle.getInt("year");
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, year);
            Date d = c.getTime();
            year = d.getYear();
        } else {
            year = new Date().getYear();
        }
        sharedPreferences = BaseActivity.sharedPref;
        reportYearlyList = ReportEngine.getInstance(sharedPreferences).getYearlyReportList(year);
        recyclerView = (RecyclerView) findViewById(R.id.activityReportYearlyRecyclerView);
        layoutManager = new LinearLayoutManager(this);
        adapter = new ReportYearlyAdapter(this, reportYearlyList, this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(ReportYearly reportYearly) {
        Intent weeklyReport = new Intent(this, ReportWeeklyActivity.class);
        Date date = new Date();
        date.setYear(reportYearly.getIntYear());
        date.setMonth(reportYearly.getIntMonth());
        weeklyReport.putExtra(ReportWeeklyActivity.DATE, date.getTime() / 1000L);
        startActivity(weeklyReport);
    }
}
