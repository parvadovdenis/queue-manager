package com.parvadov.denys.queuemanager.activities;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.parvadov.denys.queuemanager.R;

import java.sql.Time;
import java.util.List;

/**
 * Created by denys on 03.04.17.
 */

public class SettingsActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    private static SharedPreferences sharedPref;
    private static SharedPreferences.Editor editor;

    public static final String AUTO_TURN_ON = "AUTO_TURN_ON";
    public static final String AUTO_TURN_OFF = "AUTO_TURN_OFF";
    public static final String AUTO_SCHEDULE = "AUTO_SCHEDULE";
    public static final String INTERVAL = "interval";
    public static final String POWER = "power";
    public static final String SEND_SMS = "send_sms";
    public static final String SIM_ID = "sim_id";
    public static final String IS_DUAL_SIM = "is_dual_sim";

    private ImageView sim0;
    private ImageView sim1;
    private TextView textSim0;
    private TextView textSim1;

    private View otherSettingsLayout;

    private Time time;

    private int choosedSimCard;

    @Override
    public int getContentLayout() {
        return R.layout.activity_settings;
    }

    @Override
    public void initViews() {
        sharedPref = BaseActivity.sharedPref;
        editor = BaseActivity.editor;
        otherSettingsLayout = findViewById(R.id.otherSettingsLayout);
        attachEditInterval();
        setupAutoTurnOnOffTime(AUTO_TURN_ON, R.id.buttonChooseTimePowerOn);
        setupAutoTurnOnOffTime(AUTO_TURN_OFF, R.id.buttonChooseTimePowerOff);
        setupSwitchButton(POWER, R.id.switchPower, "Power");
        setupSwitchButton(SEND_SMS, R.id.switchSms, "Send SMS");
        setupSwitchButton(AUTO_SCHEDULE, R.id.switchAutoSchedule, "Auto turn");


        sim0 = (ImageView) findViewById(R.id.sim0);
        sim1 = (ImageView) findViewById(R.id.sim1);

        int chosenSimId = sharedPref.getInt(SIM_ID, -1);
        clickSim(chosenSimId);

        textSim0 = (TextView) findViewById(R.id.textSim0);
        textSim1 = (TextView) findViewById(R.id.textSim1);

        sim0.setOnClickListener(this);
        sim1.setOnClickListener(this);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager subscriptionManager = SubscriptionManager.from(this);
            Log.i("denys", "SDK is >= LoliPop");
            List<SubscriptionInfo> list = subscriptionManager.getActiveSubscriptionInfoList();
            if (list.size() == 2) {
                textSim0.setText(list.get(0).getDisplayName());
                textSim1.setText(list.get(1).getDisplayName());
                editor.putBoolean(IS_DUAL_SIM, true);
            } else {
                findViewById(R.id.simCardChooseContainer).setVisibility(View.GONE);
                editor.putBoolean(IS_DUAL_SIM, false);
            }
        } else {
            findViewById(R.id.simCardChooseContainer).setVisibility(View.GONE);
            editor.putBoolean(IS_DUAL_SIM, false);
        }

    }

    private void displayOtherSettingsLayout(boolean isDisplay) {
        if (isDisplay)
            otherSettingsLayout.setVisibility(View.VISIBLE);
        else
            otherSettingsLayout.setVisibility(View.GONE);
    }

    private void setupSwitchButton(final String shared, final int switchId, final String message) {
        boolean bool = sharedPref.getBoolean(shared, false);
        if (switchId == R.id.switchPower)
            displayOtherSettingsLayout(bool);
        showAutoScheduleContainer(bool);
        Switch mSwitch = (Switch) findViewById(switchId);
        mSwitch.setChecked(bool);
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String toast;
                if (isChecked) {
                    editor.putBoolean(shared, true);
                    toast = message + " is ON";
                } else {
                    toast = message + " is OFF";
                    editor.putBoolean(shared, false);
                }
                if (switchId == R.id.switchAutoSchedule) {
                    showAutoScheduleContainer(isChecked);
                }
                editor.commit();
                Toast.makeText(getApplicationContext(), toast, Toast.LENGTH_SHORT).show();
                if (switchId == R.id.switchPower)
                    displayOtherSettingsLayout(isChecked);
                if (switchId == R.id.switchAutoSchedule) {
                    TextView autoTurnOffTextView = (TextView) findViewById(R.id.autoTurnOffTextView);
                    if (isChecked) autoTurnOffTextView.setText("Auto turn ON");
                    else autoTurnOffTextView.setText("Auto turn OFF");
                }
            }
        });
    }

    private void attachEditInterval() {
        final EditText editInterval = (EditText) findViewById(R.id.editInterval);
        String interval = sharedPref.getString(INTERVAL, null);
        if (interval != null) {
            editInterval.setText(interval);
        }
        editInterval.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String interval = String.valueOf(editInterval.getText());
                if (actionId == KeyEvent.KEYCODE_ENDCALL) {
                    Toast.makeText(SettingsActivity.this, "Interval is " + interval, Toast.LENGTH_SHORT).show();
                    editor.putString(INTERVAL, interval);
                    editor.commit();
                }
                return false;
            }
        });
    }

    private void setupAutoTurnOnOffTime(final String shared, int buttonId) {
        final TextView setTime = (TextView) findViewById(buttonId);
        String savedTime = sharedPref.getString(shared, "");
        if (!"".equals(savedTime)) {
            setTime.setText(savedTime);
        }
        setTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(SettingsActivity.this);
                LayoutInflater factory = getLayoutInflater();
                View dialogChooseTime = factory.inflate(R.layout.dialog_choose_time, null);
                final TimePicker timePicker = (TimePicker) dialogChooseTime.findViewById(R.id.timePicker);
                timePicker.setIs24HourView(true);
                time = new Time(System.currentTimeMillis());
                timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                    @Override
                    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                        time = new Time(hourOfDay, minute, 0);
                    }
                });
                alert.setView(dialogChooseTime);
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int intHours = time.getHours();
                        int intMin = time.getMinutes();
                        String strHours;
                        String strMin;
                        if (intHours == 0) strHours = "00";
                        else strHours = String.valueOf(intHours);
                        if (intMin == 0) strMin = "00";
                        else strMin = String.valueOf(intMin);
                        setTime.setText(strHours + " : " + strMin);
                        editor.putString(shared, String.valueOf(intHours + " : " + intMin)).commit();
                    }
                });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                final AlertDialog dialog = alert.create();
                dialog.show();
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.switchAutoSchedule:
                showAutoScheduleContainer(isChecked);
                editor.putBoolean(AUTO_SCHEDULE, isChecked).commit();
                break;
        }
    }

    private void showAutoScheduleContainer(boolean show) {
        if (show)
            findViewById(R.id.settingsActivitySchedule).setVisibility(View.VISIBLE);
        else findViewById(R.id.settingsActivitySchedule).setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        chooseSimCard(v.getId());
    }

    private void chooseSimCard(int simId) {
        switch (simId) {
            case R.id.sim0:
                clickSim(0);
                break;
            case R.id.sim1:
                clickSim(1);
                break;
        }
    }

    private void clickSim(int id) {
        if (id == 0 || id == -1) {
            choosedSimCard = 0;
            sim0.setImageResource(R.drawable.icon_sim_chosen);
            sim1.setImageResource(R.drawable.icon_sim);
            editor.putInt(SIM_ID, choosedSimCard).commit();
        } else if (id == 1) {
            choosedSimCard = 1;
            sim1.setImageResource(R.drawable.icon_sim_chosen);
            sim0.setImageResource(R.drawable.icon_sim);
            editor.putInt(SIM_ID, choosedSimCard).commit();
        }
    }
}
