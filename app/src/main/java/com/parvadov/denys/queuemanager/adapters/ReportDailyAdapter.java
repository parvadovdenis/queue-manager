package com.parvadov.denys.queuemanager.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parvadov.denys.queuemanager.R;
import com.parvadov.denys.queuemanager.models.ReportDaily;
import com.parvadov.denys.queuemanager.views.myTextView.MyTextView;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by denys on 12.04.17.
 */

public class ReportDailyAdapter extends RecyclerView.Adapter<ReportDailyAdapter.ViewHolder> {
    private ArrayList<ReportDaily> list;
    private Context context;
    private OnClickListener listener;

    public ReportDailyAdapter(Context context, ArrayList<ReportDaily> list, OnClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.list = list;
    }

    @Override
    public ReportDailyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_report_daily, parent, false);
        ReportDailyAdapter.ViewHolder vh = new ReportDailyAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.bind(list.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(list.get(position));
            }
        });
    }

    public interface OnClickListener {
        void onClick(ReportDaily reportDaily);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View activityReportDailyLayout;
        private MyTextView reportDailyTextViewDay;
        private MyTextView reportDailyTextViewCustomersCount;
        private MyTextView reportDailyTextViewDate;

        public ViewHolder(View v) {
            super(v);
            activityReportDailyLayout = v.findViewById(R.id.activityReportDailyLayout);
            reportDailyTextViewDay = (MyTextView) v.findViewById(R.id.reportDailyTextViewDay);
            reportDailyTextViewCustomersCount = (MyTextView) v.findViewById(R.id.reportDailyTextViewCustomersCount);
            reportDailyTextViewDate = (MyTextView) v.findViewById(R.id.reportDailyTextViewDate);
        }

        public void bind(ReportDaily reportDaily) {
            reportDailyTextViewDay.setText(getDay(reportDaily.getDay()));
            reportDailyTextViewCustomersCount.setText(reportDaily.getCustomersCount() + " visitors");
            reportDailyTextViewDate.setText(new SimpleDateFormat("dd.MM.yyyy").format(reportDaily.getDate()));
        }

        public String getDay(int day) {
            String strMonth = new DateFormatSymbols().getWeekdays()[day];
            String result = strMonth.substring(0, 1).toUpperCase() + strMonth.substring(1);
            return result;
        }
    }
}
