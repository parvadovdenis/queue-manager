package com.parvadov.denys.queuemanager.models;

import java.util.Date;

/**
 * Created by denys on 13.04.17.
 */

public class ReportDaily {
    private int year;
    private int month;
    private int week;
    private int day;
    private int customersCount;
    private Date date;

    public ReportDaily(int year, int month, int day, int customersCount, int week, Date date) {
        this.year = year;
        this.date = date;
        this.week = week;
        this.month = month;
        this.day = day;
        this.customersCount = customersCount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getCustomersCount() {
        return customersCount;
    }

    public void setCustomersCount(int customersCount) {
        this.customersCount = customersCount;
    }
}
