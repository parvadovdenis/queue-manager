package com.parvadov.denys.queuemanager.activities;

import com.parvadov.denys.queuemanager.R;

/**
 * Created by denys on 09.04.17.
 */

public class InstructionsActivity extends BaseActivity {
    private final int layout = R.layout.activity_instructions;

    @Override
    public int getContentLayout() {
        return layout;
    }

    @Override
    public void initViews() {

    }
}
