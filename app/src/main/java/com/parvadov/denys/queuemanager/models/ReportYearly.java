package com.parvadov.denys.queuemanager.models;

import java.text.DateFormatSymbols;

/**
 * Created by denys on 11.04.17.
 */

public class ReportYearly {
    private String month;
    private int customersCount;
    private int intMonth;
    private int intYear;

    public void setCustomersCount(int customersCount) {
        this.customersCount = customersCount;
    }

    public int getIntMonth() {
        return intMonth;
    }

    public int getIntYear() {
        return intYear;
    }

    public ReportYearly(int customersCount, int intMonth, int intYear) {
        this.customersCount = customersCount;
        this.intMonth = intMonth;
        this.month = getMonth(intMonth).toUpperCase();
        this.intYear = intYear;
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month];
    }

    public String getMonth() {
        return month;
    }

    public int getCustomersCount() {
        return customersCount;
    }
}
