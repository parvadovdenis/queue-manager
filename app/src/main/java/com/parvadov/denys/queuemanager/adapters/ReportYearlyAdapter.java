package com.parvadov.denys.queuemanager.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parvadov.denys.queuemanager.R;
import com.parvadov.denys.queuemanager.models.ReportYearly;
import com.parvadov.denys.queuemanager.views.myTextView.MyTextView;

import java.util.ArrayList;

/**
 * Created by denys on 11.04.17.
 */

public class ReportYearlyAdapter extends RecyclerView.Adapter<ReportYearlyAdapter.ViewHolder> {
    private ArrayList<ReportYearly> list;
    private Context context;
    private OnClickListener listener;

    public ReportYearlyAdapter(Context context, ArrayList<ReportYearly> list, OnClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.list = list;
    }

    @Override
    public ReportYearlyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_report_yearly, parent, false);
        ReportYearlyAdapter.ViewHolder vh = new ReportYearlyAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.bind(list.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(list.get(position));
            }
        });
    }

    public interface OnClickListener {
        void onClick(ReportYearly reportYearly);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View activityReportYearlyLayout;
        private MyTextView reportYearlyTextViewTextViewMonth;
        private MyTextView reportYearlyTextViewCustomersCount;

        public ViewHolder(View v) {
            super(v);
            activityReportYearlyLayout = v.findViewById(R.id.activityReportYearlyLayout);
            reportYearlyTextViewTextViewMonth = (MyTextView) v.findViewById(R.id.reportYearlyTextViewMonth);
            reportYearlyTextViewCustomersCount = (MyTextView) v.findViewById(R.id.reportYearlyTextViewCustomersCount);
        }

        public void bind(ReportYearly reportYearly) {
            reportYearlyTextViewTextViewMonth.setText(reportYearly.getMonth());
            reportYearlyTextViewCustomersCount.setText(reportYearly.getCustomersCount() + " visitors");
        }
    }
}
