package com.parvadov.denys.queuemanager.views.myTextView;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

/**
 * Created by denys on 21.03.17.
 */

public class MyTextView extends android.support.v7.widget.AppCompatTextView {
    Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "avenir-lt-std-65-medium-58cf50e701a55.otf");

    public MyTextView(Context context) {
        super(context);
        this.setTypeface(tf);
    }

    public MyTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(tf);
    }

    public MyTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setTypeface(tf);
    }

}
