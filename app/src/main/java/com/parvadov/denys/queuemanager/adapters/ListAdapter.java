package com.parvadov.denys.queuemanager.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parvadov.denys.queuemanager.R;
import com.parvadov.denys.queuemanager.models.Customer;
import com.parvadov.denys.queuemanager.views.myTextView.MyTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Denys Parvadov on 05.03.17.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private ArrayList<Customer> list;
    private Context context;

    public ListAdapter(Context context, ArrayList<Customer> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.bind(list.get(position));
    }

    public interface OnClickListener {
        void onClick(String imagePath);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private MyTextView phoneContent;
        private MyTextView nameContent;
        private MyTextView numberContent;
        private MyTextView createTime;

        public ViewHolder(View v) {
            super(v);
            phoneContent = (MyTextView) v.findViewById(R.id.phoneContent);
            nameContent = (MyTextView) v.findViewById(R.id.nameContent);
            numberContent = (MyTextView) v.findViewById(R.id.numberContent);
            createTime = (MyTextView) v.findViewById(R.id.createTime);
        }

        public void bind(Customer customer) {
            phoneContent.setText(customer.getPhone());
            nameContent.setText(customer.getName());
            numberContent.setText(String.valueOf(customer.getNumber()));
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm a");
            createTime.setText(dateFormat.format(customer.getDateCreate()));
        }
    }
}
