package com.parvadov.denys.queuemanager.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.NumberPicker;

import com.parvadov.denys.queuemanager.R;
import com.parvadov.denys.queuemanager.engines.ReportEngine;
import com.parvadov.denys.queuemanager.views.myTextView.MyTextView;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by denys on 09.04.17.
 */

public class ReportActivity extends BaseActivity implements View.OnClickListener {
    private final int layout = R.layout.activity_report;
    private MyTextView reportDailyTextView;
    private MyTextView reportWeeklyTextView;
    private MyTextView reportYearlyTextView;
    private MyTextView reportDailyDate;
    private MyTextView reportWeeklyDate;
    private MyTextView reportYearlyDate;
    private SharedPreferences sharedPref;
    private ReportEngine reportEngine;

    @Override
    public int getContentLayout() {
        return layout;
    }

    @Override
    public void initViews() {
        sharedPref = BaseActivity.sharedPref;
        reportEngine = ReportEngine.getInstance(sharedPref);
        reportDailyTextView = (MyTextView) findViewById(R.id.reportDailyTextView);
        reportWeeklyTextView = (MyTextView) findViewById(R.id.reportWeeklyTextView);
        reportYearlyTextView = (MyTextView) findViewById(R.id.reportYearlyTextView);

        reportDailyDate = (MyTextView) findViewById(R.id.reportDailyDate);
        reportWeeklyDate = (MyTextView) findViewById(R.id.reportWeeklyDate);
        reportYearlyDate = (MyTextView) findViewById(R.id.reportYearlyDate);
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        reportDailyDate.setText(format.format(date));
        reportWeeklyDate.setText("Week " + calendar.get(Calendar.WEEK_OF_MONTH));
        reportYearlyDate.setText(String.valueOf(calendar.get(Calendar.YEAR)));

        reportDailyTextView.setText("Today you had " + reportEngine.createDailyReport() + " customers");
        reportWeeklyTextView.setText("This week you had " + reportEngine.createWeeklyReport() + " customers");
        reportYearlyTextView.setText("This year you had " + reportEngine.createYearlyReport() + " customers");
        findViewById(R.id.activityReportDaily).setOnClickListener(this);
        findViewById(R.id.activityReportWeekly).setOnClickListener(this);
        findViewById(R.id.activityReportYearly).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activityReportDaily:
                Intent activityReportDaily = new Intent(this, ReportDailyActivity.class);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                activityReportDaily.putExtra(ReportDailyActivity.DATE, calendar.getTime().getTime() / 1000L);
                startActivity(activityReportDaily);
                break;
            case R.id.activityReportWeekly:
                Intent activityReportWeekly = new Intent(this, ReportWeeklyActivity.class);
                Date date = new Date();
                activityReportWeekly.putExtra(ReportWeeklyActivity.DATE, date.getTime() / 1000L);
                startActivity(activityReportWeekly);
                break;
            case R.id.activityReportYearly:
                enterYear();
                break;

        }
    }

    private void enterYear() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.dialog_year_picker, null);
        final NumberPicker numberPicker = (NumberPicker) view.findViewById(R.id.numberPicker);
        numberPicker.setMaxValue(Calendar.getInstance().get(Calendar.YEAR));
        numberPicker.setMinValue(Calendar.getInstance().get(Calendar.YEAR) - 10);
        numberPicker.setValue(numberPicker.getMaxValue());
        numberPicker.setWrapSelectorWheel(false);
        setDividerColor(numberPicker);
        numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        alertDialog.setView(view);
//        alertDialog.setIcon(R.drawable.key);

        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent activityReportYearly = new Intent(ReportActivity.this, ReportYearlyActivity.class);
                        activityReportYearly.putExtra("year", numberPicker.getValue());
                        startActivity(activityReportYearly);
                    }
                });

        alertDialog.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    private void setDividerColor(NumberPicker picker) {
        Field[] numberPickerFields = NumberPicker.class.getDeclaredFields();
        for (Field field : numberPickerFields) {
            if (field.getName().equals("mSelectionDivider")) {
                field.setAccessible(true);
                try {
                    field.set(picker, getResources().getDrawable(R.drawable.test));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
}

