package com.parvadov.denys.queuemanager.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.parvadov.denys.queuemanager.R;
import com.parvadov.denys.queuemanager.adapters.ReportWeeklyAdapter;
import com.parvadov.denys.queuemanager.engines.ReportEngine;
import com.parvadov.denys.queuemanager.models.ReportWeekly;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by denys on 12.04.17.
 */

public class ReportWeeklyActivity extends BaseActivity implements ReportWeeklyAdapter.OnClickListener {
    public static final String DATE = "DATE";
    private final int layout = R.layout.activity_report_weekly;
    private Date date;
    private SharedPreferences sharedPreferences;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<ReportWeekly> reportWeeklyList;

    @Override
    public int getContentLayout() {
        return layout;
    }

    @Override
    public void initViews() {
        sharedPreferences = BaseActivity.sharedPref;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(DATE)) {
            date = new Date(bundle.getLong(DATE) * 1000L);
        }
        reportWeeklyList = ReportEngine.getInstance(sharedPref).getWeeklyReportList(date);
        recyclerView = (RecyclerView) findViewById(R.id.activityReportWeeklyRecyclerView);
        layoutManager = new LinearLayoutManager(this);
        adapter = new ReportWeeklyAdapter(this, reportWeeklyList, this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        ((TextView) findViewById(R.id.activityReportWeeklyTextViewTitle)).setText(getMonth(calendar.get(Calendar.MONTH)) + ", " + calendar.get(Calendar.YEAR));
    }

    public String getMonth(int month) {
        String strMonth = new DateFormatSymbols().getMonths()[month];
        String result = strMonth.substring(0, 1).toUpperCase() + strMonth.substring(1);
        return result;
    }

    @Override
    public void onClick(ReportWeekly reportWeekly) {
        Intent dailyReportActivity = new Intent(this, ReportDailyActivity.class);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.WEEK_OF_MONTH, reportWeekly.getWeekNumber());
        int a = (reportWeekly.getWeekNumber() - 1) * 7;
        if (reportWeekly.getWeekNumber() == 1) {
            a = 1;
        } else if (reportWeekly.getWeekNumber() == calendar.getMaximum(Calendar.WEEK_OF_MONTH)) {
            a = calendar.getMaximum(Calendar.DAY_OF_MONTH) - 1;
        }
        calendar.set(Calendar.DATE, calendar.getFirstDayOfWeek() * a);
        Date d = calendar.getTime();
        dailyReportActivity.putExtra(ReportDailyActivity.DATE, calendar.getTime().getTime() / 1000L);
        startActivity(dailyReportActivity);
    }
}
