package com.parvadov.denys.queuemanager.models;

/**
 * Created by denys on 12.04.17.
 */

public class ReportWeekly {
    private int weekNumber;
    private int customersCount;

    public ReportWeekly(int weekNumber, int customersCount) {
        this.weekNumber = weekNumber;
        this.customersCount = customersCount;
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getCustomersCount() {
        return customersCount;
    }

    public void setCustomersCount(int customersCount) {
        this.customersCount = customersCount;
    }
}
