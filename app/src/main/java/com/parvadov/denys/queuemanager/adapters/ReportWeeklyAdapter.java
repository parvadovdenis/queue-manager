package com.parvadov.denys.queuemanager.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parvadov.denys.queuemanager.R;
import com.parvadov.denys.queuemanager.models.ReportWeekly;
import com.parvadov.denys.queuemanager.views.myTextView.MyTextView;

import java.util.ArrayList;

/**
 * Created by denys on 12.04.17.
 */

public class ReportWeeklyAdapter extends RecyclerView.Adapter<ReportWeeklyAdapter.ViewHolder> {
    private ArrayList<ReportWeekly> list;
    private Context context;
    private OnClickListener listener;

    public ReportWeeklyAdapter(Context context, ArrayList<ReportWeekly> list, OnClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.list = list;
    }

    @Override
    public ReportWeeklyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_report_weekly, parent, false);
        ReportWeeklyAdapter.ViewHolder vh = new ReportWeeklyAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.bind(list.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(list.get(position));
            }
        });
    }

    public interface OnClickListener {
        void onClick(ReportWeekly reportWeekly);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View activityReportYearlyLayout;
        private MyTextView reportWeeklyTextViewWeek;
        private MyTextView reportWeeklyTextViewCustomersCount;

        public ViewHolder(View v) {
            super(v);
            activityReportYearlyLayout = v.findViewById(R.id.activityReportWeeklyLayout);
            reportWeeklyTextViewWeek = (MyTextView) v.findViewById(R.id.reportWeeklyTextViewWeek);
            reportWeeklyTextViewCustomersCount = (MyTextView) v.findViewById(R.id.reportWeeklyTextViewCustomersCount);
        }

        public void bind(ReportWeekly reportWeekly) {
            reportWeeklyTextViewWeek.setText("Week " + reportWeekly.getWeekNumber());
            reportWeeklyTextViewCustomersCount.setText(reportWeekly.getCustomersCount() + " visitors");
        }
    }
}
