package com.parvadov.denys.queuemanager.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.parvadov.denys.queuemanager.R;
import com.parvadov.denys.queuemanager.adapters.ReportDailyAdapter;
import com.parvadov.denys.queuemanager.engines.ReportEngine;
import com.parvadov.denys.queuemanager.models.ReportDaily;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by denys on 13.04.17.
 */

public class ReportDailyActivity extends BaseActivity implements ReportDailyAdapter.OnClickListener {
    private final int layout = R.layout.activity_report_daily;
    public static final String DATE = "DATE";
    private Date date;
    private SharedPreferences sharedPreferences;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<ReportDaily> reportDailyList;
    private Calendar calendar;

    @Override
    public int getContentLayout() {
        return layout;
    }

    @Override
    public void initViews() {
        sharedPreferences = BaseActivity.sharedPref;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(DATE)) {
            date = new Date(bundle.getLong(DATE) * 1000L);
        }
        reportDailyList = ReportEngine.getInstance(sharedPref).getDailyReportList(date);
        recyclerView = (RecyclerView) findViewById(R.id.activityReportDailyRecyclerView);
        layoutManager = new LinearLayoutManager(this);
        adapter = new ReportDailyAdapter(this, reportDailyList, this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        calendar = Calendar.getInstance();
        calendar.setTime(date);
        ((TextView) findViewById(R.id.activityReportDailyTextViewTitle)).setText(getMonth(calendar.get(Calendar.MONTH)) + ", week " +
                calendar.get(Calendar.WEEK_OF_MONTH) + ", " + calendar.get(Calendar.YEAR));
    }

    public String getMonth(int month) {
        String strMonth = new DateFormatSymbols().getMonths()[month];
        String result = strMonth.substring(0, 1).toUpperCase() + strMonth.substring(1);
        return result;
    }

    @Override
    public void onClick(ReportDaily reportDaily) {
        Intent activityHistory = new Intent(this, HistoryActivity.class);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, reportDaily.getYear());
        c.set(Calendar.MONTH, reportDaily.getMonth());
        c.set(Calendar.WEEK_OF_MONTH, reportDaily.getWeek());
        c.set(Calendar.DAY_OF_WEEK, reportDaily.getDay());
        Date d = reportDaily.getDate(); //TODO
        activityHistory.putExtra("test", reportDaily.getDate().getTime() / 1000L);
        startActivity(activityHistory);
    }
}
