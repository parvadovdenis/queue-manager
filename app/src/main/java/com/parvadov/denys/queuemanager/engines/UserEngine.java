package com.parvadov.denys.queuemanager.engines;

import android.content.SharedPreferences;

import com.parvadov.denys.queuemanager.models.Customer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by denys on 09.04.17.
 */

public class UserEngine extends BaseEngine {
    public static UserEngine userEngine;
    public static final String NAME = "name";
    public static final String PHONE = "phone";
    public static final String NUMBER = "number";
    public static final String TIME = "time";
    public static final String DATE = "date";
    public static final String LIST = "list";
    public static final String NOT_SHOW_IN_LIST = "NOT_SHOW_IN_LIST";
    public static final String CUSTOMER_LIST = "customer_list";
    public static Date currentDate;

    protected UserEngine(SharedPreferences sharedPreferences, SharedPreferences.Editor editor) {
        super(sharedPreferences, editor);
    }

    public static UserEngine getInstance(SharedPreferences sharedPreferences, SharedPreferences.Editor editor) {
        currentDate = new Date();
        if (userEngine == null) {
            userEngine = new UserEngine(sharedPreferences, editor);
            return userEngine;
        } else {
            return userEngine;
        }
    }

    public void removeCustomer(Customer customerForRemove) {
        String phone = customerForRemove.getPhone();
        String name = customerForRemove.getName();
        String number = String.valueOf(customerForRemove.getNumber());

        JSONArray jsonArray = null;
        String json = sharedPref.getString(CUSTOMER_LIST, "null");
        JSONObject customer;
        try {
            int index = 0;
            jsonArray = new JSONArray(json);
            JSONObject removedDateJsonObject = getDateJsonObjectFromArray(jsonArray, currentDate);
            JSONArray listJson = removedDateJsonObject.getJSONArray(LIST);
            long time = 0;
            for (int i = 0; i < listJson.length(); i++) {
                customer = listJson.getJSONObject(i);
                if (name.equals(customer.getString(NAME))
                        && phone.equals(customer.getString(PHONE))
                        && number.equals(customer.getString(NUMBER))) {
                    index = i;
                    time = customer.getLong(TIME);
                }
            }
            listJson.remove(index);

            JSONObject newCustomer = new JSONObject();
            newCustomer.put(NAME, name);
            newCustomer.put(NUMBER, number);
            newCustomer.put(PHONE, phone);
            newCustomer.put(NOT_SHOW_IN_LIST, true);
            newCustomer.put(TIME, time);

            listJson.put(newCustomer);
            removedDateJsonObject.put(LIST, listJson);
            jsonArray.remove(getIndexFromJsonArray(jsonArray, currentDate));
            jsonArray.put(removedDateJsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        editor.putString(CUSTOMER_LIST, String.valueOf(jsonArray));
        editor.commit();
    }

    public void saveCustomer(String phone, String name, String number) {
        currentDate = new Date();
        JSONArray jsonArray = null;
        String json = sharedPref.getString(CUSTOMER_LIST, "null");
        JSONObject customer;
        try {
            customer = new JSONObject();
            customer.put(NAME, name);
            customer.put(PHONE, phone);
            customer.put(TIME, String.valueOf(System.currentTimeMillis() / 1000L));
//            customer.put(NUMBER, number);
            if (!json.equals("null")) {
                jsonArray = new JSONArray(json);
            } else {
                jsonArray = new JSONArray();
            }

            if (!checkJsonArrayForDate(jsonArray, new Date())) {
                JSONObject jsonDate = new JSONObject();
                jsonDate.put(DATE, currentDate.getTime() / 1000L);
                JSONArray newArray = new JSONArray();
                customer.put(NUMBER, "1");
                newArray.put(customer);
                jsonDate.put(LIST, newArray);
                jsonArray.put(jsonDate);
            } else {
                JSONObject existingJsonObject = getDateJsonObjectFromArray(jsonArray, new Date());
                jsonArray.remove(getIndexFromJsonArray(jsonArray, new Date()));
                JSONArray existingList = existingJsonObject.getJSONArray(LIST);
                if (number.equals("0") || number.equals("-1")) {
                    int newNumber;
                    try {
                        newNumber = Integer.parseInt(existingList.getJSONObject(existingList.length() - 1)
                                .getString(NUMBER)) + 1;
                    } catch (JSONException e) {
                        newNumber = 1;
                    }
                    customer.put(NUMBER, String.valueOf(newNumber));
                } else customer.put(NUMBER, number);
                existingList.put(customer);
                existingJsonObject.put(LIST, existingList);
                jsonArray.put(existingJsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        editor.putString(CUSTOMER_LIST, String.valueOf(jsonArray));
        editor.commit();
    }

    public JSONObject getDateJsonObjectFromArray(JSONArray array, Date date) {
        JSONObject result = null;
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = null;
            Date jsonDate = null;
            try {
                object = array.getJSONObject(i);
                jsonDate = new Date(object.getLong("date") * 1000);
            } catch (JSONException e) {
                e.printStackTrace();
                result = null;
                return null;
            }
            if (date.getYear() == jsonDate.getYear()
                    && date.getMonth() == jsonDate.getMonth()
                    && date.getDate() == jsonDate.getDate())
                return object;
        }
        return result;
    }

    public String getPreviousCustomerNumber(SharedPreferences sharedPref) {
        JSONArray jsonArray = null;
        String json = sharedPref.getString(CUSTOMER_LIST, "null");
        int result;
        try {
            if (!json.equals("null")) {
                jsonArray = new JSONArray(json);
                JSONArray customersList = getDateJsonObjectFromArray(jsonArray, new Date()).getJSONArray(LIST);
                result = Integer.parseInt(customersList.getJSONObject(customersList.length() - 1).getString(NUMBER));
            } else {
                jsonArray = new JSONArray();
                result = 0;
            }

        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
            result = 0;
        }
        return String.valueOf(result + 1);
    }

    public int getIndexFromJsonArray(JSONArray array, Date date) {
        int result = -1;
        for (int i = 0; i < array.length(); i++) {
            JSONObject object;
            Date jsonDate = null;
            try {
                object = array.getJSONObject(i);
                jsonDate = new Date(object.getLong("date") * 1000);
            } catch (JSONException e) {
                e.printStackTrace();
                result = -1;
            }
            if (date.getYear() == jsonDate.getYear()
                    && date.getMonth() == jsonDate.getMonth()
                    && date.getDay() == jsonDate.getDay())
                return i;
        }
        return result;
    }

    public boolean checkJsonArrayForDate(JSONArray array, Date date) {
        boolean result = false;
        for (int i = 0; i < array.length(); i++) {
            JSONObject object;
            Date jsonDate = null;
            try {
                object = array.getJSONObject(i);
                jsonDate = new Date(object.getLong("date") * 1000);
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
            if (date.getYear() == jsonDate.getYear()
                    && date.getMonth() == jsonDate.getMonth()
                    && date.getDay() == jsonDate.getDay())
                return true;
        }
        return result;
    }
}
